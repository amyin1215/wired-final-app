package com.otracks;
 
import java.util.ArrayList;
import java.util.HashMap;
 
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
 
public class InboxAdapter extends BaseAdapter {
 
    private Activity activity;
    public ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    private long id;
 
    public InboxAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    public int getCount() {
        return data.size();
    }
 
    public Object getItem(int position) {
        return data.get(position);
    }
 
    public long getItemId(int position) {
        return id;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_row, null);
        Resources res = activity.getResources();
        int empty_gray = res.getColor(R.color.empty_measure);
        TypedArray selections;
        int category = position % 4;
        int color;
		if (MainActivity.isCoffee) {
			TypedArray categoryArrays = res.obtainTypedArray(R.array.coffee_palette_color_arrays);
			selections = res.obtainTypedArray(categoryArrays.getResourceId(category, R.array.coffee_colors));
			color = selections.getColor(0, empty_gray);
			categoryArrays.recycle();
		} else {
			// fruittart only has one selection for each category
			selections = res.obtainTypedArray(R.array.fruittart_colors);
			color = selections.getColor(category, empty_gray);
		}
        
		// get background of list_row (border_listview)
		GradientDrawable shapeDrawable = (GradientDrawable)vi.getBackground();
		shapeDrawable.setStroke(2, color); 
		selections.recycle();
	        	
        TextView sender = (TextView)vi.findViewById(R.id.sender); // sender
        TextView message_text = (TextView)vi.findViewById(R.id.title); // message body
        TextView date = (TextView)vi.findViewById(R.id.date); // timestamp
        
        HashMap<String, String> message = new HashMap<String, String>();
        message = data.get(position);
        
        // Format date 2009-10-01 10:04:22
        String datetext = message.get(CompositionsFragment.KEY_DATE);
        String dte = datetext.split(" ")[0];
        String time = datetext.split(" ")[1];
        datetext = time.split(":")[0] + ":" + time.split(":")[1] + " " + dte.split("-")[1] + "/" + dte.split("")[2] ;
 
        // Set values in listview for each composition
        sender.setText(message.get(CompositionsFragment.KEY_EMAIL));
        message_text.setText(message.get(CompositionsFragment.KEY_TITLE));
        date.setText(datetext);
	
		return vi;
    }
}