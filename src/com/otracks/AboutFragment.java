package com.otracks;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class AboutFragment extends Fragment implements OnClickListener {
	private static final String TAG = "About";
	View view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		Log.e(TAG, "About Page Launched");
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.about, container, false);
		locationSetup();
		setBackgroundOpacity();
		return view;
	}
	
	private void locationSetup() {
		
		Resources res = getActivity().getResources();
		String toLocation = res.getString(R.string.paris);
		if (MainActivity.isParis) {
			toLocation = res.getString(R.string.london);
		}
		String text = String.format(res.getString(R.string.about), toLocation);
		CharSequence styledText = Html.fromHtml(text);
		((TextView)view.findViewById(R.id.about_text)).setText(styledText);
	}

	private void setBackgroundOpacity() {
		View backgroundimage = view.findViewById(R.id.ophone_background);
		Drawable background = backgroundimage.getBackground();
		background.setAlpha(80);
		backgroundimage.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		((MainActivity)getActivity()).showCompositions();
		
	}
}
