package com.otracks;

public class User{

    private int _id;
    private String _name;
    private String _palette;
    private String _location;

    public User(){
        this._id = 0;
        this._name = "";
    }

    public void setId(int id){
        this._id = id;
    }

    public int getId(){
        return this._id;
    }

    public void setName(String name){
        this._name = name;
    }

    public String getName(){
        return this._name;
    }
    
    public void setPalette(String palette) {
    	this._palette = palette;
    }
    
    public String getPalette(){
    	return this._palette;
    }

    public void setLocation(String location) {
    	this._location = location;
    }
    
    public String getLocation(){
    	return this._location;
    }
}