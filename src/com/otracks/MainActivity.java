package com.otracks;

import static com.otracks.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.otracks.CommonUtilities.EXTRA_MESSAGE;
import static com.otracks.CommonUtilities.EXTRA_SYMPHONYID;
import static com.otracks.CommonUtilities.SENDER_ID;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gcm.GCMRegistrar;
import com.otracks.CompositionsFragment.SearchCompositionsTask;
import com.otracks.aesthetics.TypefaceSpan;
import com.otracks.bluetoothcoffee.BluetoothChatService;
import com.otracks.bluetoothcoffee.DeviceListActivity;
import com.otracks.bluetoothcoffee.OPhoneClass;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {
	private static final String TAG = "MainActivity";
	static OPhoneClass oPhone;
	private final static boolean D = true;
	public final static int CATEGORYMAX = 4;
	public final int SCENTDURATION = 12000;
	
    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_REGISTER = 3;
    public static final int SEND_ONOTE = 20;
      
    AboutFragment aboutFragment;
    FragmentTransaction ft;
    public static boolean isCoffee;
    public static boolean isParis = false;
    
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private BluetoothChatService bt = null;
    
    AlertDialogManager alert = new AlertDialogManager();
    AsyncTask<Void, Void, Void> mRegisterTask;
      
    public static String name;
    public static String location;
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setupAboutFragment();
	    
	    registerReceiver(mHandleMessageReceiver, new IntentFilter(
                DISPLAY_MESSAGE_ACTION));
	    
	    setupBluetoothAdapter();
	    new GetPaletteTask().execute("");
	}
	
	class GetPaletteTask extends AsyncTask<String, Void, String> {
	    @Override
	    protected String doInBackground(String... empty_params) {
	    	final String regId = GCMRegistrar.getRegistrationId(MainActivity.this);
	    	User user = CoffeeServer.getAccountInfo(regId, MainActivity.this);
			return (user != null) ? user.getPalette() : "coffee";
	    }  
	    @Override
	    protected void onPostExecute(String palette) {
	    	
	    	isCoffee = !(palette != null && palette.equals("fruittart"));
	    	isParis = !(palette != null && palette.equals("London"));
	    	Log.e(TAG, "isCoffee = " + isCoffee);
	    }
	}
	
	public void setupAboutFragment() {
		// get an instance of FragmentTransaction from your Activity 
        ft = getSupportFragmentManager().beginTransaction();

        //add a fragment 
        aboutFragment = new AboutFragment();
        ft.replace(R.id.container, aboutFragment);
        ft.addToBackStack(null);
        ft.commit();        
	}

	public void showCompositions() {
		// Create new fragment and transaction
		Fragment newFragment = new CompositionsFragment();
		ft = getSupportFragmentManager().beginTransaction();

		// Replace whatever is in the fragment_container view with this fragment,
		// and add the transaction to the back stack
		ft.replace(R.id.container, newFragment);
		ft.addToBackStack(null);

		// Commit the transaction
		ft.commit();
	}
	
	/**
     * Receiving push messages
     * */
    protected final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());
	        if (intent.getExtras().getString(EXTRA_SYMPHONYID) != null) {
	            // Showing Onote if symphony id   
	            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
	            new GetCompositionByIdTask().execute(new Intent[]{intent});
	        } else {
	            Toast.makeText(getApplicationContext(), "Device Registered: " + message, Toast.LENGTH_LONG).show();	        	
	        }
            // Releasing wake lock
            WakeLocker.release();
        }
    };
	
	class GetCompositionByIdTask extends AsyncTask<Intent, Void, JSONArray> {
	    @Override
	    protected JSONArray doInBackground(Intent... params) {
			String symphony_id = params[0].getStringExtra(EXTRA_SYMPHONYID) != null ? 
					params[0].getStringExtra(EXTRA_SYMPHONYID) : "2"; 
			return CoffeeServer.getCompositionById(symphony_id);
	    }  
	    @Override
	    protected void onPostExecute(JSONArray jArray) {	
	    	Log.e(TAG, "GetCompositionByIdTask jarray is " + jArray);
       		try {
       			if (jArray == null) return;
				JSONObject json_data = jArray.getJSONObject(0);
				String[] movements = {json_data.getString("mvmt0"),json_data.getString("mvmt1"),
				                      json_data.getString("mvmt2"),json_data.getString("mvmt3")};
	        	viewComposition(json_data.getString("composition_id"),
		            	json_data.getString("title"),
		            	json_data.getString("email"),
		            	json_data.getString("date"),
		            	movements
		            );
	        	Log.e("get composition by id: ","json_data: " + json_data.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
	    }
	}
	
	public void viewComposition(String symphony_id, String title, String email, String date, String[] movements) {
		ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out);
		ViewCompositionFragment newFragment = 
				ViewCompositionFragment.newInstance(symphony_id, movements, title, email, date);
        
        ft.replace(R.id.container, newFragment, "ViewComposition");
		ft.addToBackStack(null);
		ft.commit();
	}
	
	// After symphony is finished playing
	public void endPage(String symphony_id) {
		ft = getSupportFragmentManager().beginTransaction();
		EndOptionsFragment newFragment = 
				EndOptionsFragment.newInstance(symphony_id);
        
        ft.replace(R.id.container, newFragment, "EndOptionsFragment");
		ft.addToBackStack(null);
		ft.commit();
	}
	
	private void setupBluetoothAdapter() {
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
	}
	
    @Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        // Otherwise, setup the chat session
        } else {
            if (bt == null) {
            	setupService();
            }
        }
    }
    
    private void setupService() {
    	 // Initialize the BluetoothChatService to perform bluetooth connections
        bt = new com.otracks.bluetoothcoffee.BluetoothChatService(this, mHandler);
        oPhone = new OPhoneClass(bt);
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (bt != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (bt.getState() == BluetoothChatService.STATE_NONE) {
              // Start the Bluetooth chat services
              bt.start();
            }
        }
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // prevent activity from being destroyed
        Log.e(TAG, "onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
    }
   
   @Override
   protected void onDestroy() {
       if (mRegisterTask != null) {
           mRegisterTask.cancel(true);
       }
       try {
           unregisterReceiver(mHandleMessageReceiver);
           GCMRegistrar.onDestroy(this);
       } catch (Exception e) {
           Log.e("UnRegister Receiver Error", "> " + e.getMessage());
       }
       // if activity is destroy for reason other than orientation
       if (!isChangingConfigurations()) {
    	   // Stop the Bluetooth chat services
    	   Log.e(TAG, "not orientation change");
    	   if (bt != null) bt.stop();
       }
       if(D) Log.e(TAG, "--- ON DESTROY ---");
       super.onDestroy();
   }
   
	 // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	Log.e(TAG, "handleMessage msg=" + msg);
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                String title = "oNotes";
                switch (msg.arg1) {
                case BluetoothChatService.STATE_CONNECTED:
                    title = getResources().getString(R.string.title_connected_to) + mConnectedDeviceName;
                    
                    break;
                case BluetoothChatService.STATE_CONNECTING:
                	title = getResources().getString(R.string.title_connecting);
                    break;
                case BluetoothChatService.STATE_LISTEN:
                case BluetoothChatService.STATE_NONE:
                	 title = getResources().getString(R.string.title_not_connected);
                    break;
                }
                setTitle(setFont(title));
                break;
            case MESSAGE_WRITE:
                byte[] writeBuf = (byte[]) msg.obj;
                // construct a string from the buffer
                //String writeMessage = new String(writeBuf);
                String writeMessage = Integer.toBinaryString((int)writeBuf[0]);
                if (D) Log.e(TAG, "MESSAGE_WRITE = " + writeMessage);
                break;
            case MESSAGE_READ:
            	 byte[] readBuf = (byte[]) msg.obj;
            	 // construct a string from the valid bytes in the buffer
            	 String readMessage = new String(readBuf, 0, msg.arg1);
            	 Log.e(TAG, "Read message:  " + readMessage);
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
	
	/* Menu with one option: Connect to OPhone via Bluetooth
	 * TODO: Add HELP option
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.options_menu, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.e(TAG, "onOptionsItemSelected");
		switch (item.getItemId()) {
	    case R.id.scan:
	        // Launch the DeviceListActivity to see devices and do scan
	    	Intent serverIntent = new Intent(this, DeviceListActivity.class);
	    	startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
	    	return true;
	    case R.id.register:
	    	Intent registerIntent = new Intent(this, RegisterActivity.class);
	    	startActivityForResult(registerIntent, REQUEST_REGISTER);
	    	return true;
		}
		return super.onOptionsItemSelected(item); 
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if(D) Log.d(TAG, "onActivityResult " + resultCode);
	    switch (requestCode) {
    	case REQUEST_CONNECT_DEVICE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                // Get the device MAC address
                String address = data.getExtras()
                                     .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                Log.e(TAG, "Address: " + address);
                // Get the BluetoothDevice object
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                // Attempt to connect to the device
                bt.connect(device);
                
            } else {
				Toast.makeText(this,  "BT not connected", Toast.LENGTH_SHORT).show();
			}
			break;
    	case REQUEST_ENABLE_BT:
    		// When the request to enable Bluetooth returns
			if (resultCode == Activity.RESULT_OK) {
			    // Bluetooth is now enabled, so set up session
				Toast.makeText(this, "Bluetooth successfully enabled", Toast.LENGTH_SHORT).show();
				setupService();
				if (D) Log.e(TAG, "btEnabled");
			} else {
                // User did not enable Bluetooth or an error occured
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
			}
			break;
    	case REQUEST_REGISTER:
    		// When the request to register to ophone.fr and Google Cloud Messaging returns
    		if (resultCode == Activity.RESULT_OK) { 
    			if (data != null)
    				register(data.getExtras().getString("name"), 
    						data.getExtras().getString("palette"),
    						data.getExtras().getString("location"));
    			else 
    				Toast.makeText(this, "Device already registered", Toast.LENGTH_SHORT).show();
    		} else {
    			Toast.makeText(this, "Device not successfully registered", Toast.LENGTH_SHORT).show();
    		}
    		break;
	    }
	}
	
	
	/*
	 * Register phone
	 */
	private void register(final String name, final String palette, final String location) {      
        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);
         
        registerReceiver(mHandleMessageReceiver, new IntentFilter(
                DISPLAY_MESSAGE_ACTION));
         
        // Get GCM registration id
        final String regId = GCMRegistrar.getRegistrationId(this);
 
        // Check if regid already presents
        if (regId.equals("")) {
            // Registration is not present, register now with GCM           
            GCMRegistrar.register(this, SENDER_ID);
        } else {
            // Device is already registered on GCM
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                // Skips registration.              
                Toast.makeText(getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();
            } else {
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                final Context context = this;
                mRegisterTask = new AsyncTask<Void, Void, Void>() {
 
                    @Override
                    protected Void doInBackground(Void... params) {
                        // Register on our server
                        // On server creates a new user
                        ServerUtilities.register(context, name, palette, location, regId);
                        return null;
                    }
 
                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }
 
                };
                mRegisterTask.execute(null, null, null);
            }
        }
	}
    
	private SpannableString setFont(String s){
		SpannableString mSS = new SpannableString(s);
		mSS.setSpan(new TypefaceSpan(this), 0, s.length(),
		        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return mSS;
	}
}
