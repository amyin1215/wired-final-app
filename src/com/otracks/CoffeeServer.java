package com.otracks;

import static com.otracks.CommonUtilities.SERVER_URL;
import static com.otracks.CommonUtilities.displayMessage;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.android.gcm.GCMRegistrar;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/*
 * Contains all functions that interact with Virtual Coffee Server
 */

public class CoffeeServer {
	public static int MAX_MVMTS = 4;
	public static String TAG = "CoffeeServer";
	public static JSONArray getCompositions(String palette){
		String result = "";
		String ServerUrl = SERVER_URL + "get_compositions.php";
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("palette", palette));
		InputStream is = null; 
		final String TAG = "CoffeeServer";
		//http post
		try{
	        HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost(ServerUrl);
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        Log.e("OSymphony", "ServerUrl: " + ServerUrl);
	        HttpResponse response = httpclient.execute(httppost);
	        if (response != null) {
	        	HttpEntity entity = response.getEntity();
	        	is = entity.getContent();
	        } else {
				Log.e(TAG, "HttpResponse null");
				return null;
			}
		}catch(Exception e){
		    Log.e("TAG", "Error in http connection "+e.toString());
		}
		//convert response to string
		try{
			if (is != null) {
		        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();		 
		        result=sb.toString();
			} else {
				Log.e(TAG, "InputStream null");
			}
		}catch(Exception e){
			Log.e("TAG", "Error converting result "+e.toString());
		}
		 
		//parse json data
		try {
			JSONArray jArray = new JSONArray(result);
	        return jArray;
		} catch(JSONException e){
			Log.e("TAG", "Error parsing data "+e.toString());
		}
		return null;
	}
	
	
	// Get inbox messages
	public static JSONArray getCompositionById(String symphony_id){
		String result = "";
		String ServerUrl = SERVER_URL + "get_composition.php";
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("symphony_id", symphony_id));
		InputStream is = null; 
		//http post
		try{
	        HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost(ServerUrl);
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        Log.e("Getting symphony by id: " + symphony_id, "ServerUrl: " + ServerUrl);
	        HttpResponse response = httpclient.execute(httppost);
	        if (response != null) {
	        	HttpEntity entity = response.getEntity();
	        	is = entity.getContent();
	        } else {
				Log.e(TAG, "HttpResponse null");
				return null;
			}
		}catch(Exception e){
		    Log.e("TAG", "Error in http connection "+e.toString());
		}
		//convert response to string
		try{
			if (is != null) {
		        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();		 
		        result=sb.toString();
			} else {
				Log.e(TAG, "InputStream null");
			}
		}catch(Exception e){
			Log.e("TAG", "Error converting result "+e.toString());
		}
		 
		//parse json data
		try {
			JSONArray jArray = new JSONArray(result);
	        return jArray;
		} catch(JSONException e){
			Log.e("TAG", "Error parsing data "+e.toString());
		}
		return null;
	}
	
	public static Boolean sendOnote(String message, String symphony_id) {
		String ServerUrl = SERVER_URL + "send_onote.php";
		Log.e(TAG, "sendOnote symphony_id = " + symphony_id);
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("palette", MainActivity.isCoffee ? "coffee" : "fruittart"));
		nameValuePairs.add(new BasicNameValuePair("location", MainActivity.isParis ? "London" : "Paris"));
		nameValuePairs.add(new BasicNameValuePair("message",message));
		nameValuePairs.add(new BasicNameValuePair("symphony_id", symphony_id));
		InputStream is = null; 
		//http post
		try{
		        HttpClient httpclient = new DefaultHttpClient();
		        HttpPost httppost = new HttpPost(ServerUrl);
		        Log.e("OMessage", "ServerUrl: " + ServerUrl);
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        HttpResponse response = httpclient.execute(httppost);
		        HttpEntity entity = response.getEntity();
		        is = entity.getContent();  
		}catch(Exception e){
		        Log.e("TAG", "Error in http connection "+e.toString());
		        return false;
		}
		Log.e("OMessage", nameValuePairs.toString());
		return true;
	}
	
	/*
	 * Send email inviting friend to Virtual Coffee Bar
	 */
	public static Boolean sendEmail(String message, String recipient) {
		String ServerUrl = SERVER_URL + "send_wired_invite.php";
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("message",message));
		nameValuePairs.add(new BasicNameValuePair("recipient_address", recipient));
		InputStream is = null; 
		//http post
		try{
		        HttpClient httpclient = new DefaultHttpClient();
		        HttpPost httppost = new HttpPost(ServerUrl);
		        Log.e("OMessage", "ServerUrl: " + ServerUrl);
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        HttpResponse response = httpclient.execute(httppost);
		        HttpEntity entity = response.getEntity();
		        is = entity.getContent();  
		}catch(Exception e){
		        Log.e("TAG", "Error in http connection "+e.toString());
		        return false;
		}
		Log.e("OMessage", nameValuePairs.toString());
		return true;
	}
	

	public static JSONArray getRecipients() {
		String result = "";
		String ServerUrl = SERVER_URL + "getExhibitionPhones.php";
		InputStream is = null; 
		//http post
		try{
		        HttpClient httpclient = new DefaultHttpClient();
		        HttpPost httppost = new HttpPost(ServerUrl);
		        Log.e("OMessage", "ServerUrl: " + ServerUrl);
		        HttpResponse response = httpclient.execute(httppost);
		        HttpEntity entity = response.getEntity();
		        is = entity.getContent();
		}catch(Exception e){
		        Log.e("TAG", "Error in http connection "+e.toString());
		}
		//convert response to string
		try{
		        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();
		 
		        result=sb.toString();
		}catch(Exception e){
		        Log.e("TAG", "Error converting result "+e.toString());
		}
		 
		//parse json data
		try {
			JSONArray jArray = new JSONArray(result);
	        for(int i=0;i<jArray.length();i++){
	        	JSONObject json_data = jArray.getJSONObject(i);
	        	Log.i("TAG","id: "+json_data.getInt("id")+
	            	", name: "+json_data.getString("name")+
	            	", email: "+json_data.getString("email")
	            );
	        }
	        return jArray;
		} catch(JSONException e){
			Log.e("TAG", "Error parsing data "+e.toString());
		}
		return null;
	}
	
    
	public static User[] populateRecipients() {
		JSONArray json_recipients = CoffeeServer.getRecipients();
		User[] users; 
		int length = json_recipients.length();
		
		if (length == 0) {
			users = new User[2];
	        users[0] = new User();
	        users[0].setId(1);
	        users[0].setName("Phone 1");
	
	        users[1] = new User();
	        users[1].setId(2);
	        users[1].setName("Phone 2");       
		}
		else {
			users = new User[length];
	        for(int i=0;i<length;i++){
	            try {
	            	JSONObject json_data = json_recipients.getJSONObject(i);
	            	users[i] = new User();
	            	users[i].setId(json_data.getInt("id"));
	            	users[i].setName(json_data.getString("name"));
				} catch (JSONException e) {
					Log.e("OMessage", "populateRecipients i=" + i);
					e.printStackTrace();
				}
	        }
		}
        return users;
	}
	
	public static User getAccountInfo(String regid, Context context){
		String result = "";
		String ServerUrl = SERVER_URL + "get_compositions_account.php";
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("regid",regid));
		InputStream is = null; 
		//http post
		try{
	        HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost(ServerUrl);
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        Log.e(TAG, "ServerUrl: " + ServerUrl);
	        HttpResponse response = httpclient.execute(httppost);
	        HttpEntity entity = response.getEntity();
	        is = entity.getContent();
		}catch(Exception e){
		    Log.e(TAG, "Error in http connection "+e.toString());
		    return null;
		}
		//convert response to string
		try{
	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	        StringBuilder sb = new StringBuilder();
	        String line = null;
	        while ((line = reader.readLine()) != null) {
	                sb.append(line + "\n");
	        }
	        is.close();
	 
	        result=sb.toString();
		}catch(Exception e){
			Log.e(TAG, "Error converting result "+e.toString());
			return null;
		}
		 
		//parse json data
		try {
			JSONObject json_data = new JSONObject(result);
	    	User user = new User();
	    	user.setId(json_data.getInt("id"));
	    	user.setName(json_data.getString("name"));
	    	user.setPalette(json_data.getString("palette"));
	    	user.setLocation(json_data.getString("location"));
	        return user;
		} catch(JSONException e){
			Log.e(TAG, "Error parsing data "+e.toString());
		}	
		return null;
	}


	public static JSONArray getCompositionsByEmail(String palette, String email) {
		String result = "";
		String ServerUrl = SERVER_URL + "get_composition_by_email.php";
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("email", email));
		nameValuePairs.add(new BasicNameValuePair("palette", palette));
		InputStream is = null; 
		//http post
		try{
	        HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost(ServerUrl);
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        Log.e("getcompositionsbyemail", nameValuePairs.toString());
	        HttpResponse response = httpclient.execute(httppost);
	        if (response != null) {
	        	HttpEntity entity = response.getEntity();
	        	is = entity.getContent();
	        } else {
				Log.e(TAG, "HttpResponse null");
				return null;
			}
		}catch(Exception e){
		    Log.e("getcompositionsbyemail", "Error in http connection "+e.toString());
		}
		//convert response to string
		try{
			if (is != null) {
		        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();		 
		        result=sb.toString();
			} else {
				Log.e(TAG, "InputStream null");
			}
		}catch(Exception e){
			Log.e(TAG, "Error converting result "+e.toString());
		}
		 
		//parse json data
		try {
			JSONArray jArray = new JSONArray(result);
	        return jArray;
		} catch(JSONException e){
			Log.e("getcompositionsbyemail", "Error parsing data "+e.toString());
		}
		return null;
	}

	public static boolean unregister(String regid){
		String result = "";
		String ServerUrl = SERVER_URL + "symphony_unregister.php";
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("regId",regid));
		InputStream is = null; 
		//http post
		try{
	        HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost(ServerUrl);
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        Log.e(TAG, "ServerUrl: " + ServerUrl);
	        HttpResponse response = httpclient.execute(httppost);
	        HttpEntity entity = response.getEntity();
	        is = entity.getContent();
		}catch(Exception e){
		    Log.e("TAG", "Error in http connection "+e.toString());
		    return false;
		}
		//convert response to string
		try{
	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	        StringBuilder sb = new StringBuilder();
	        String line = null;
	        while ((line = reader.readLine()) != null) {
	                sb.append(line + "\n");
	        }
	        is.close();
	 
	        result=sb.toString();
		}catch(Exception e){
			Log.e(TAG, "Error converting result "+e.toString());
			return false;
		}
		 
		//parse json data
		try {
			JSONObject json_data = new JSONObject(result);
			Log.e(TAG, "unregister json_data = " + json_data);
	    	return json_data.getBoolean("isUnregistered");
		} catch(JSONException e){
			Log.e(TAG, "Error parsing data "+e.toString());
		}
		return false;
	}
}