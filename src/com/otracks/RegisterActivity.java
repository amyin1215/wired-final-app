package com.otracks;

import com.google.android.gcm.GCMRegistrar;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/*
 * Code adapted from http://www.androidhive.info/2012/10/android-push-notifications-using-google-cloud-messaging-gcm-php-and-mysql/
 * 
 * @author AndroidHive (@RaviTamada) and Amy Yin (@yin_amy)
 * @date 05/13/2013
 * 
 * RegisterActivity.java
 * Registers Phone with Google and Ophone.fr/gcm_server_php/ to receive messages via Google Cloud Messaging
 * 
 * 
 */
public class RegisterActivity extends Activity {
    // alert dialog manager
    AlertDialogManager alert = new AlertDialogManager();
 
    // Internet detector
    ConnectionDetector cd;
 
    // UI elements
    EditText txtName;
    EditText txtPalette;
    EditText txtLocation;
 
    // Register button
    Button btnRegister;
    
	class GetAccountInfoTask extends AsyncTask<String, User, User> {
	    @Override
	    protected User doInBackground(String... params) {
	    	return CoffeeServer.getAccountInfo(params[0], RegisterActivity.this);
	    }  
	    @Override
	    protected void onPostExecute(User user) {
	    	if (user == null) {
		    	txtName.setText("Unable to connect");    	
		    	txtPalette.setText("Unable to connect");
		    	txtLocation.setText("Unable to connect");
	    		alert.showAlertDialog(RegisterActivity.this,
	                    "Server Error",
	                    "OPHONE is having difficulty finding your device's id", false);
	    		Log.e("onPostExecute", "user null");
	    	} else {
	    		Log.e("onPostExecute", "user not null");
		    	txtName.setText(user.getName());    	
		    	txtPalette.setText(user.getPalette());
		    	txtLocation.setText(user.getLocation());
		    	MainActivity.isCoffee = !(user.getPalette() != null && user.getPalette().equals("fruittart"));
		    	MainActivity.isParis = !(user.getLocation() != null && user.getLocation().equals("London"));
		    	// Enable Unregister Button and Disable Unregister button
		    	findViewById(R.id.btnRegister).setEnabled(false);
		    	findViewById(R.id.btnUnregister).setEnabled(true);
	    	}
	    }
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		
        // Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);
        
    	setContentView(R.layout.register_activity);
        txtName = (EditText) findViewById(R.id.txtName);
        txtPalette = (EditText) findViewById(R.id.txtPalette);
        txtLocation = (EditText) findViewById(R.id.txtLocation);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        
    	if (GCMRegistrar.isRegisteredOnServer(this)) {
    		setResult(Activity.RESULT_OK);
    		btnRegister.setText("Already Registered");
    		btnRegister.setEnabled(false);
    		String regId = GCMRegistrar.getRegistrationId(this);
    		Log.e("RegisterActivity", "GCM id = " + regId);
    		if (regId != "") 
    			new GetAccountInfoTask().execute(regId);
    		
    	} else {
    		setResult(Activity.RESULT_CANCELED);
    		findViewById(R.id.btnUnregister).setEnabled(false);
	        cd = new ConnectionDetector(this);
	 
	        // Check if Internet present
	        if (!cd.isConnectingToInternet()) {
	            // Internet Connection is not present
	            alert.showAlertDialog(this,
	                    "Internet Connection Error",
	                    "Please connect to working Internet connection", false);
	            // stop executing code by return
	            return;
	        }
	        setupRegister();
    	}
    } 
	
	private void setupRegister() {
	
		 /*
	     * Click event on Register button
	     **/
	    btnRegister.setOnClickListener(new View.OnClickListener() {
	
	        @Override
	        public void onClick(View arg0) {
	            // Read EditText data
	            String name = txtName.getText().toString();
	            String palette = txtPalette.getText().toString();
	            String location = txtLocation.getText().toString(); // TODO: Dropdown for location
	            Log.e("RegisterActivity", "Register name: " + name + "palette: " + palette);
	
	            // Check if user filled the form
	            if(name.trim().length() > 0 && palette.trim().length() > 0){
	                // Create the result Intent and include the MAC address
	                Intent i = new Intent();
	                i.addCategory("Register");
	                
	                // Sending registration details to ScentActivity
	                i.putExtra("name", name);
	                i.putExtra("palette", palette);
	                i.putExtra("location", location);
	
	                // Set result and finish this Activity
	                setResult(Activity.RESULT_OK, i);
	                finish();
	            }else{
	                // user didn't filled in data
	                // request form to be filled in
	                alert.showAlertDialog(RegisterActivity.this, "Registration Error!", "Please enter your details", false);
	            }
	        }
	    });
	}
	/* 
	 * Unregister GCM
	 * TODO: Remove from ophone.fr database also
	 */
	public void unregister(View view) {
		new UnregisterTask().execute("");
		setResult(Activity.RESULT_CANCELED);
	}
	   
	class UnregisterTask extends AsyncTask<String, Void, Boolean> {
	    @Override
	    protected Boolean doInBackground(String... params) {
	    	return ServerUtilities.unregister(RegisterActivity.this, GCMRegistrar.getRegistrationId(RegisterActivity.this));	    	
	    }  
	    @Override
	    protected void onPostExecute(Boolean isUnregistered) {
	    	if (isUnregistered) {
		    	txtName.setText("");
		    	txtPalette.setText("");
		    	txtLocation.setText("");
		    	Toast.makeText(RegisterActivity.this, "Unregistered!", Toast.LENGTH_SHORT).show();
		    	// Enable Register Button and Disable Unregister button
		    	btnRegister.setEnabled(true);
		    	setupRegister();
		    	btnRegister.setText("Register");
		    	findViewById(R.id.btnUnregister).setEnabled(false);
	    	}
	    }
	}
}