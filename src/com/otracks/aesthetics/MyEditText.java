package com.otracks.aesthetics;

import android.content.Context;
import android.widget.EditText;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class MyEditText extends EditText{
    public MyEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyEditText(Context context) {
        super(context);
    }
  
    public void setTypeface(Typeface tf, int style) {
    	if (!isInEditMode()) {
	        if (style == Typeface.BOLD) {
	        	// xml preview does not load custom fonts
				super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/dosis_light.ttf"), style);	
	        } else {
	        	TypefaceSpan span = new TypefaceSpan(getContext());
	            Typeface typef = span.getTypeface(getContext());
	            super.setTypeface(typef, style);
	        }
    	}
//    	} else {
//    		super.setTypeface(tf, style);
//    	}
   }

}

