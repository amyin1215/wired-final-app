package com.otracks;

import com.google.android.gcm.GCMRegistrar;
import com.otracks.CoffeeServer;
import com.otracks.aesthetics.TypefaceSpan;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class OnoteActivity extends Activity {//implements OnClickListener{
	Button sendbtn;
	public static int receiver_id = 0;
	public String message = "Default message";
	public int scentid;
	static ArrayAdapter<CharSequence> scentsAdapter;
	private final boolean D = true;
	private final boolean USE_GCM = true;
	private final int DAVID_PHONE = 24;
	private final int AMY_PHONE = 19;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setTitle(setFont("Send an oNote"));
//		Intent intent = getIntent();
//		scentid = intent.getIntExtra("scentid", 0);
//		setContentView(R.layout.sendnote);
//	    sendbtn = (Button) findViewById(R.id.send_btn);
//	    sendbtn.setOnClickListener(this);
//	    setMessageSend();
//	    // dismiss keyboard by touching outside edittext box
//	    LinearLayout layout = (LinearLayout)findViewById(R.id.layout);
//	    layout.setOnClickListener(this);
//	     
//        // Populate scents spinner
//	    setScentsSpinner();
	}
	
//	private void setMessageSend() {
//		
//		com.otracks.aesthetics.MyEditText message_input = (com.otracks.aesthetics.MyEditText)findViewById(R.id.message_input);
//	    message_input.setOnClickListener(this);
//	    TextView.OnEditorActionListener exampleListener = new TextView.OnEditorActionListener(){
//
//			@Override
//			public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event) {
//				Log.e("Onote", "onEditor");
//				if (actionId == EditorInfo.IME_ACTION_SEND) {
//					sendMessage();
//					return true;
//				} 
//				return false;
//			}
//	    	
//	    };
//	    message_input.setOnEditorActionListener(exampleListener);
//		
//	}
//	
//	private void setScentsSpinner() {
//        Spinner scentsSpinner = (Spinner) findViewById(R.id.scents_spinner);
//        scentsAdapter = ArrayAdapter.createFromResource(this,
//                R.array.scents_array, R.layout.spinner);
//        scentsAdapter.setDropDownViewResource(R.layout.spinner);
//        scentsSpinner.setAdapter(scentsAdapter);
//        
//        scentsSpinner.setSelection(scentid);
//        scentsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view,
//                    int pos, long id) {
//                // Here you get the current item (a User object) that is selected by its position
//                scentid = pos;
//                Log.e("Onote", "Selected scent id: " + pos);
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> adapter) {  }
//        });
//	}
//
//	@Override
//	public void onClick(View v) {
//		switch (v.getId()){
//		case R.id.message_input:
//			// do nothing if edittext box clicked
//			break;
//		case R.id.send_btn:
//			sendMessage();
//			break;
//		case R.id.layout:
//			hideSoftKeyboard(OnoteActivity.this);
//		}
//	}
//	
//	private class SendScentmailTask extends AsyncTask<String, Void, String> {
//		@Override
//		protected String doInBackground(String... params) {
//			if (USE_GCM) {
//				
//				String sender_regid = GCMRegistrar.getRegistrationId(OnoteActivity.this);
//				// 
//				receiver_id = params[3].equalsIgnoreCase("Amy") ? AMY_PHONE : DAVID_PHONE;
//				// params: String message, int scent_id, int receiver_id, String sender_regid
//				CoffeeServer.sendScentmail(params[0], 0, receiver_id, sender_regid);
//			} else {
//				//  params = {message, sendToAddress, fromName, toName}
//				CoffeeServer.sendEmail(params[0], params[1], params[2], params[3]);
//				Log.e("Onote", "sendEmail");
//			}
//			return params[3];
//			
//			
//		}
//	}
//	
//	private void sendMessage(){
//		com.otracks.aesthetics.MyEditText msg = (com.otracks.aesthetics.MyEditText) findViewById(R.id.message_input);
//		message = msg.getText().toString();
//		String sendToAddress = USE_GCM ? "" : ((com.otracks.aesthetics.MyEditText)findViewById(R.id.sendTo)).getText().toString() ;
//		String fromName = USE_GCM ? "" : ((com.otracks.aesthetics.MyEditText)findViewById(R.id.sendFrom)).getText().toString();
//		String toName = ((com.otracks.aesthetics.MyEditText)findViewById(R.id.toName)).getText().toString(); 
//		new SendScentmailTask().execute(message, sendToAddress, fromName, toName);
//		
//        // Set result and finish this Activity
//        setResult(Activity.RESULT_OK);
//		finish(); // dismiss dialog
//	}
// 
//	public void composeEmail(String message, int scentid, String[] sendTo, String toName, String fromName) {
//		String scentName = getResources().getStringArray(R.array.scents_array)[scentid];
//		String body = getResources().getString(R.string.email_body, fromName, toName, scentName, message);
//		Intent emailIntent = new Intent();
//		emailIntent.setAction(Intent.ACTION_SEND);
//		emailIntent.putExtra(Intent.EXTRA_EMAIL, sendTo);
//		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "LaboCafe: The Virtual Coffee Exhibition");
//		emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(body));
//		emailIntent.setType("text/html");
//		startActivityForResult(Intent.createChooser(emailIntent, "Send " + scentName), ScentActivity.SEND_ONOTE);
//	}
//	
//    public static void hideSoftKeyboard(Activity activity) {
//        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
//    }
//    
//	private SpannableString setFont(String s){
//		SpannableString mSS = new SpannableString(s);
//		mSS.setSpan(new TypefaceSpan(this), 0, s.length(),
//		        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		return mSS;
//	}
}
