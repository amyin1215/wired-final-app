package com.otracks;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EndOptionsFragment extends Fragment implements OnClickListener {
	private static final String TAG = "EndOptions";
	View view;
	private static String id;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		Log.e(TAG, "End Options Page Launched");
		id = getArguments().getString("symphony_id");
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.end_options_fragment, container, false);
		locationSetup();
		setBackgroundOpacity();
		setupOnoteButton();
		setupInviteButton();
		return view;
	}
	
	public static EndOptionsFragment newInstance(String symphony_id) {
		EndOptionsFragment myFragment = new EndOptionsFragment();
	    Bundle args = new Bundle();
	    args.putString("symphony_id", symphony_id);
	    myFragment.setArguments(args);

	    return myFragment;
	}
	
	private void setupOnoteButton() {
		view.findViewById(R.id.sendOnote).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Get Message from editext and send
				String message = ((EditText)view.findViewById(R.id.message)).getText().toString();
				new SendOnoteTask().execute(new String[]{message, id});
				Toast.makeText(getActivity(), "Your message \"" + message + "\" has been sent!", 
						Toast.LENGTH_LONG).show();
			}
		});
	}
	
	class SendOnoteTask extends AsyncTask<String, Void, Boolean> {
	    @Override
	    protected Boolean doInBackground(String... params) {
	    	return CoffeeServer.sendOnote(params[0], params[1]); 
	    }  
	    @Override
	    protected void onPostExecute(Boolean wasSent) {
			clearFields();
	    }
	}
	
	class SendEmailTask extends AsyncTask<String, Void, Boolean> {
	    @Override
	    protected Boolean doInBackground(String... params) {
	    	return CoffeeServer.sendEmail(params[0], params[1]); // message, recipient
	    }  
	    @Override
	    protected void onPostExecute(Boolean wasSent) {
			
	    }
	}
	private void setupInviteButton() {
		view.findViewById(R.id.invite).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Get Message from edittext and send
				String message = ((EditText)view.findViewById(R.id.message)).getText().toString();
				String email = ((EditText)view.findViewById(R.id.email)).getText().toString();
				new SendEmailTask().execute(new String[]{message, email});
				Toast.makeText(getActivity(), "Your message \"" + message + "\" has been sent to " + email + "!",
						Toast.LENGTH_LONG).show();
				clearFields();
			}
			
		});
	}
	
	private void clearFields() {
		((EditText)view.findViewById(R.id.message)).setText("");
		((EditText)view.findViewById(R.id.email)).setText("");
	}
	
	private void locationSetup() {
		
		Resources res = getActivity().getResources();
		String toLocation = "Paris";
		if (MainActivity.isParis) { // send to opposite location
			toLocation = "London";
		}
		String text = String.format(res.getString(R.string.send_onote), toLocation);
		CharSequence styledText = Html.fromHtml(text);
		((TextView)view.findViewById(R.id.sendOnote)).setText(styledText);
	}

	private void setBackgroundOpacity() {
		View backgroundimage = view.findViewById(R.id.end_options_background);
		Drawable background = backgroundimage.getBackground();
		background.setAlpha(80);
		backgroundimage.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		((MainActivity)getActivity()).setupAboutFragment();
		
	}
}
